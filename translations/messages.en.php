<?php

return array(
    'homepage.title' => 'Curvy Chic Woman',
    'login.title' => 'Curvy Chic Woman',
    'homepage.text' => 'WELCOME',

    'login' => 'Login',
    'username' => 'Username',
    'password' => 'Password',
    'login.welcome_message' => 'Welcome',
    'login.welcome_message_2' => 'login to enter find out more',
    'choose_an_option' => 'Choose an option',
    'remember_me' => 'Remember me',

    'menu.users_title_list' => 'Users',
    'menu.users_add' => 'Add',
    'menu.users_manage' => 'Manage',
    'menu.user_profile' => 'Profile',
    'menu.logout' => 'Logout',
    'menu.edit' => 'Edit',
    'menu.users_list' => 'List',
    'menu.products_title' => 'Products',
    'menu.products_list' => 'List',
    'menu.products_add' => 'Add',

    'user.password' => 'Password',
    'user.repeatPassword' => 'Repeat Password',
    'save' => 'Save',
    'user.email' => 'Email',
    'user.username' => 'Username',
    'user.role' => 'Role',

    'role.admin' => 'Admin',
    'role.ROLE_ADMIN' => 'Admin',
    'role.user' => 'User',
    'role.ROLE_USER' => 'User',

    'user.edit_profile_title' => 'Edit profile',
    'user.add_new_title' => 'Register a new user',
    'user.list_users_title' => 'All users',
    'user.list_username' => 'Username',
    'user.list_users_email' => 'Email',
    'user.list_users_is_active' => 'Active',
    'user.list_users_role' => 'Role',
    'user.list_users_actions' => 'Actions',
    'user.list_users_last_login' => 'Last login',
    'user.edit' => 'Edit',
    'user.is_active' => 'Active',
    'user.is_disabled' => 'Disabled',
    'user.status' => 'Status',
    'user.name' => 'Name',
    'user.first_name' => 'First'
);
