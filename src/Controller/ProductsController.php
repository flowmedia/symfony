<?php
/**
 * Created by PhpStorm.
 * User: lau
 * Date: 06.04.2018
 * Time: 00:19
 */

namespace App\Controller;

use App\Entity\Product;
use App\Form\Product\ProductAdd;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class ProductsController extends Controller
{
    /**
     * @Route("/admin/product/list", name="product_list")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function list()
    {
        return $this->render('products/list.html.twig', []);
    }

    /**
     * @Route("/admin/product/product_list_ajax", name="product_list_ajax")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"POST", "GET"})
     */

    public function productListAjax(Request $request, TranslatorInterface $translator)
    {
        $sEcho = $request->request->get('sEcho');

        $filters['limit'] = $request->request->get('iDisplayLength');
        $filters['offset'] = $request->request->get('iDisplayStart');
        $filters['like'] = $request->request->get('sSearch');

        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository(Product::class)->getProducts($filters);
        $totalUsers = $em->getRepository(Product::class)->countProducts($filters);
        $totalUsersNoFilter = $em->getRepository(Product::class)->countProducts();

        $output = [
            "sEcho" => $sEcho,
            "iTotalRecords" => $totalUsersNoFilter,
            "iTotalDisplayRecords" => $totalUsers,
            "aaData" => [],
        ];

        foreach ($users as $user) {
            $row = [];
            $row[] = $user->getId();
            $row[] = $user->getName();
            $row[] = $user->getType();
            $row[] = $user->getClickBankId();
            $row[] = '<a type="button" href="' . $this->get('router')->generate('product_edit', ['id' => $user->getId()]) . '" class="btn btn-success">' . $translator->trans("user.edit") . '</a>';

            $output['aaData'][] = $row;
        }

        return new JsonResponse($output);

    }

    /**
     * @Route("/admin/products", name="product_edit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function edit()
    {


        return $this->render('admin/homepage.html.twig', []);
    }

    /**
     * @Route("/admin/product/add", name="product_add")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function add(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(ProductAdd::class, $product);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {


            // 4) save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('product_list');
        }
        return $this->render('products/add.html.twig', [
            'form' => $form->createView(),
            'error' => false,
        ]);
    }



}