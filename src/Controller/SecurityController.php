<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\User\UserLoginType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use App\Form\User\UserType;
use App\Form\User\UserEdit;
use App\Form\User\UserEditAdmin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class SecurityController extends Controller
{

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authUtils, TranslatorInterface $translator)
    {
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        $form = $this->createForm( UserLoginType::class );

        return $this->render('login/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/register", name="user_registration")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            // 4) save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('login');
        }

        return $this->render(
            'login/register.html.twig',
            array('form' => $form->createView(),
                'error'         => ''
            )
        );
    }

    /**
     * @Route("/admin/user_list", name="user_list")
     * @Security("has_role('ROLE_ADMIN')")
     */

    public function userList()
    {



        return $this->render(
            'login/list_users.html.twig'

        );
    }
    /**
     * @Route("/admin/user_list_ajax", name="user_list_ajax")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"POST", "GET"})
     */

    public function userListAjax(Request $request, TranslatorInterface $translator)
    {
        $sEcho = $request->request->get('sEcho');

        $filters['limit'] = $request->request->get('iDisplayLength');
        $filters['offset'] = $request->request->get('iDisplayStart');
        $filters['like'] = $request->request->get('sSearch');

        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository( User::class )->getUsers($filters);
        $totalUsers = $em->getRepository( User::class )->countUsers($filters);
        $totalUsersNoFilter = $em->getRepository( User::class )->countUsers();

        $output = [
            "sEcho" => $sEcho,
            "iTotalRecords" => $totalUsersNoFilter,
            "iTotalDisplayRecords" => $totalUsers,
            "aaData" => []
        ];

        foreach ($users as $user) {
            $row = [];
            $row[] = $user->getId();
            $row[] = $user->getEmail();
            $row[] = ($user->getIsActive())? $translator->trans("user.is_active") :$translator->trans("user.is_disabled");
            $row[] = $translator->trans("role.".$user->getRole());
            $row[] = $user->getLastLogin()->format('Y-m-d H:i:s');
            $row[] = '<a type="button" href="'.$this->get('router')->generate('user_edit_admin', array('id' => $user->getId())).'" class="btn btn-success">'.$translator->trans("user.edit").'</a>';

            $output['aaData'][] = $row;
        }

        return new JsonResponse($output);

    }
    /**
     * @Route("/admin/edit", name="user_edit")
     * @Security("has_role('ROLE_USER')")
     */
    public function editProfile(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = $this->getUser();

        // 1) build the form
        $form = $this->createForm(UserEdit::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            // 4) save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('login');
        }

        return $this->render(
            'login/edit_profile.html.twig',
            array('form' => $form->createView(),
                'error'         => ''
            )
        );
    }

    /**
     * @Route("/admin/editAdmin/{id}", name="user_edit_admin")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editUserProfile(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {

        $userId = $request->attributes->get('id');
        $user= $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userId);
        $userPassword = $user->getPassword();

        // 1) build the form
        $form = $this->createForm(UserEditAdmin::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            if ($user->getPassword() === null) {
                $user->setPassword($userPassword);
            } else {
                $password = $passwordEncoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);
            }

            // 4) save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('user_list');
        }

        return $this->render(
            'login/edit_profile.html.twig',
            array('form' => $form->createView(),
                'error'         => ''
            )
        );
    }

}
