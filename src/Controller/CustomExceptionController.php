<?php

namespace App\Controller;

use Symfony\Bundle\TwigBundle\Controller\ExceptionController;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class CustomExceptionController extends ExceptionController
{

    public function __construct( Environment $twig, bool $debug)
    {
        parent::__construct($twig, $debug);
    }

    public function showException(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null)
    {

        $currentContent = $this->getAndCleanOutputBuffering($request->headers->get('X-Php-Ob-Level', -1));

        $code = $exception->getStatusCode();
        $format = $request->getRequestFormat();

        return new Response($this->twig->render(
            (string) 'Exception/error'.$code.'.'.$format.'.twig',
            array(
                'status_code' => $code,
                'status_text' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
                'exception' => $exception,
                'logger' => $logger,
                'currentContent' => $currentContent,
            )
        ), 200, array('Content-Type' => $request->getMimeType($request->getRequestFormat()) ?: 'text/html'));

    }
}