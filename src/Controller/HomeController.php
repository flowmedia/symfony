<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\LogHelper;
use Symfony\Component\HttpFoundation\JsonResponse;

class HomeController extends Controller
{

    /**
     * @Route("/", name="index")
     *
     */
    public function index(LogHelper $logHelper)
    {

//        $logHelper->log('homepage');
//        $logHelper->logTrait();

        return $this->render('home/homepage.html.twig', array(
            'page_title' => 'Welcome!'
        ));
    }


}