<?php
namespace App\Form\User;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserLoginType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults( array(
            'data_class'      => User::class,
            'csrf_protection' => true,
            'csrf_field_name' => '_csrf_token',
            'csrf_token_id'   => 'authenticate',
        ) );

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, ['label' => 'username', 'required' => true, 'attr'     => ['placeholder' => 'username' ]])
            ->add('password', PasswordType::class, ['label' => 'password', 'required' => true, 'attr'     => ['placeholder' => 'password' ]])
            ->add('_remember_me', CheckboxType::class, [ 'label'    => 'remember_me', 'required' => false, ])
            ->add('save', SubmitType::class, ['label' => 'login'])
        ;
    }

}