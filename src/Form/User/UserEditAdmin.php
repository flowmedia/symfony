<?php
namespace App\Form\User;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UserEditAdmin extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'user.name'
            ])
            ->add('firstName', TextType::class, [
                'label' => 'user.first_name'
            ])
            ->add('email', EmailType::class, [
                'label' => 'user.email',
                'attr'   =>  ['class'   => '']
            ])
            ->add('username', TextType::class, [
                'label' => 'user.username'
            ])
            ->add('Password', RepeatedType::class, array(
                'required' => false,
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'user.password', 'attr' => array('autocomplete' => 'nope')),
                'second_options' => array('label' => 'user.repeatPassword', 'attr' => array('autocomplete' => 'nope')),
            ))
            ->add('role', ChoiceType::class, array(
                'placeholder' => 'choose_an_option',
                'choices'  => array(
                    'role.admin' => 'ROLE_ADMIN',
                    'role.user' => 'ROLE_USER'
                ),
                'label' => 'user.role'
            ))
            ->add('isactive', ChoiceType::class, array(
                'placeholder' => 'choose_an_option',
                'choices'  => array(
                    'user.is_active' => '1',
                    'user.is_disabled' => '0'
                ),
                'label' => 'user.status'
            ))
            ->add('save', SubmitType::class, [
                'label' => 'save',
                'attr'   =>  ['class'   => 'float-right btn btn-success']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'csrf_protection' => true,
            'csrf_field_name' => '_csrf_token',
            'csrf_token_id'   => 'authenticate',
        ));
    }
}