<?php
namespace App\Form\User;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'user.email',
                'attr'   =>  ['class'   => '']
            ])
            ->add('username', TextType::class, [
                'label' => 'user.username'
            ])
            ->add('Password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'user.password'),
                'second_options' => array('label' => 'user.repeatPassword'),
            ))
            ->add('role', ChoiceType::class, array(
                'placeholder' => 'choose_an_option',
                'choices'  => array(
                    'role.admin' => 'ROLE_ADMIN',
                    'role.user' => 'ROLE_USER'
                ),
                'label' => 'user.role'
            ))
            ->add('save', SubmitType::class, [
                'label' => 'save',
                'attr'   =>  ['class'   => 'float-right btn btn-success']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'csrf_protection' => true,
            'csrf_field_name' => '_csrf_token',
            'csrf_token_id'   => 'authenticate',
        ));
    }
}