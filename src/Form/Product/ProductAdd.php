<?php
namespace App\Form\Product;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ProductAdd extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'product.name',
                'attr'   =>  ['class'   => '']
            ])
            ->add('description', TextType::class, [
                'label' => 'product.description',
                'attr'   =>  ['class'   => '']
            ])
            ->add('click_bank_id', IntegerType::class, [
                'label' => 'product.clickBankId',
                'attr'   =>  ['class'   => '']
            ])
            ->add('type', ChoiceType::class, array(
                'placeholder' => 'choose_an_option',
                'choices'  => array(
                    'product.one_payment' => '1',
                    'product.payment_plan' => '2',
                    'product.recursive_payment' => '3',
                ),
                'label' => 'product.type'
            ))
            ->add('price', MoneyType::class,[
                'label' => 'product.price'
            ])
            ->add('save', SubmitType::class, [
                'label' => 'save',
                'attr'   =>  ['class'   => 'float-right btn btn-success']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Product::class,
            'csrf_protection' => true,
            'csrf_field_name' => '_csrf_token',
            'csrf_token_id'   => 'product',
        ));
    }
}