<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 *
 * @Table(name="admin_product")
 *
 * @UniqueEntity(
 *     fields={"click_bank_id"},
 *     message="This Clickbank Id is already added."
 * )
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(1)
     */
    private $click_bank_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="float")
     *
     * @Assert\NotBlank()
     */
    private $price;


    /**
     * @ORM\Column(type="boolean")
     */
    private $deleted;

    /**
     * Product constructor.
     */
    public function __construct() {
        $this->deleted = 0;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId( $id ): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName( $name ): void {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription( $description ): void {
        $this->description = $description;
    }







    /**
     * @return mixed
     */
    public function getType() {
        return $this->type;
    }


    /**
     * @param mixed $type
     */
    public function setType( $type ): void {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice( $price ): void {
        $this->price = $price;
    }


    /**
     * @return mixed
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * @param mixed $deleted
     */
    public function setDeleted( $deleted ): void {
        $this->deleted = $deleted;
    }

    /**
     * @return mixed
     */
    public function getClickBankId()
    {
        return $this->click_bank_id;
    }

    /**
     * @param mixed $click_bank_id
     */
    public function setClickBankId($click_bank_id): void
    {
        $this->click_bank_id = $click_bank_id;
    }

}
