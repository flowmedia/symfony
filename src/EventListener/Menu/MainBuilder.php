<?php

namespace App\EventListener\Menu;

use App\Event\ConfigureMenuEvent;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MainBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function build(FactoryInterface $factory)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Dashboard', array('route' => '_acp_dashboard'));

        $this->container->get('event_dispatcher')->dispatch(
            ConfigureMenuEvent::CONFIGURE,
            new ConfigureMenuEvent($factory, a)
        );

        return $menu;
    }
}