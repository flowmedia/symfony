<?php

namespace App\EventListener\Menu;


use Sonata\AdminBundle\Event\ConfigureMenuEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class MenuBuilderListener
{
    protected $authChecker;

    public function __construct(AuthorizationCheckerInterface $authChecker)
    {
        $this->authChecker = $authChecker;
    }

    public function addMenuItems(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();

        if ($this->authChecker->isGranted('ROLE_ADMIN')) {
            $levelUsers = $menu->addChild('Products', [ 'label' => 'menu.products_title'])->setExtras([ 'icon' => '<i class="fa fa-database"></i>', ]);
            $levelUsers->addChild('product_add',[ 'label' => 'menu.products_add', 'route' => 'product_add', ])->setExtras([ 'icon' => '<i class="fa fa-plus"></i>', ]);
            $levelUsers->addChild('product_list',[ 'label' => 'menu.products_list', 'route' => 'product_list', ])->setExtras([ 'icon' => '<i class="fa fa-list"></i>', ]);
        }

        if ($this->authChecker->isGranted('ROLE_ADMIN')) {
            $levelUsers = $menu->addChild('users', [ 'label' => 'menu.users_title_list'])->setExtras([ 'icon' => '<i class="fa fa-users"></i>', ]);
            $levelUsers->addChild('users_add',[ 'label' => 'menu.users_add', 'route' => 'user_registration', ])->setExtras([ 'icon' => '<i class="fa fa-plus"></i>', ]);
            $levelUsers->addChild('users_list',[ 'label' => 'menu.users_list', 'route' => 'user_list', ])->setExtras([ 'icon' => '<i class="fa fa-list"></i>', ]);
        }

        $profile = $menu->addChild('profile', [ 'label' => 'menu.user_profile'])->setExtras([ 'icon' => '<i class="fa fa-user-circle"></i>', ]);
        $profile->addChild('edit',[ 'label' => 'menu.edit', 'route' => 'user_edit', ])->setExtras([ 'icon' => '<i class="fa fa-edit"></i>', ]);
        $profile->addChild('logout',[ 'label' => 'menu.logout', 'route' => 'logout', ])->setExtras([ 'icon' => '<i class="fa fa-sign-out"></i>', ]);


    }
}