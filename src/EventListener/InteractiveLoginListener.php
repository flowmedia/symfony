<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Doctrine\ORM\EntityManager;
use App\Entity\User;

/**
 * Listener that updates the last login of the user
 */
class InteractiveLoginListener {

    protected $em;
    protected $request;

    public function __construct( EntityManager $em, RequestStack $request ) {
        $this->em      = $em;
        $this->request = $request;
    }

    /**
     * Update the user "lastLogin" on login
     */
    public function onSecurityInteractiveLogin( InteractiveLoginEvent $event ) {

        $user = $event->getAuthenticationToken()->getUser();

        if ( $user instanceof User ) {
            if ( $this->request->getCurrentRequest()->hasSession() ) {
                $user->setLastLogin( new \DateTime( 'now' ) );
                $this->em->persist( $user );
                $this->em->flush();
            }
        }
    }
}