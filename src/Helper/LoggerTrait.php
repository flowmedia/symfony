<?php
/**
 * Created by PhpStorm.
 * User: lau
 * Date: 06.03.2018
 * Time: 22:39
 */

namespace App\Helper;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

trait LoggerTrait
{
    /**
     * @var LoggerInterface|null
     */
    private $logger;

    /**
     * @var Request
     */
    private $request;


    /**
     * @required
     */
    public function setLogger(LoggerInterface $trackingLogger, RequestStack $request)
    {
        $this->logger = $trackingLogger;
        $this->request = $request;
    }

    private function logInfo(string $message, array $context = [])
    {
        if ($this->logger && $this->request) {

            $ipAddress = $this->request->getCurrentRequest()->getClientIp();
            $browser = $this->request->getCurrentRequest()->headers->get('USER-AGENT');

            $this->logger->info($message, [
                'IpAddress' => $ipAddress,
                'Browser' => $browser
            ]);
        }
    }
}