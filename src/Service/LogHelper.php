<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use App\Helper\LoggerTrait;

class LogHelper
{
    use LoggerTrait;

    /**
     * @var LoggerInterface
     */
    private $trackingLogger;
    /**
     * @var Request
     */
    private $request;

    public function __construct(LoggerInterface $trackingLogger, RequestStack $request)
    {

        $this->trackingLogger = $trackingLogger;
        $this->request = $request;
    }

    public function log(string $source): void
    {

        $ipAddress = $this->request->getCurrentRequest()->getClientIp();
        $browser = $this->request->getCurrentRequest()->headers->get('USER-AGENT');

        $logData = 'IpAddress: ' . $ipAddress . ' Browser: ' .$browser. 'Page: ' . $source;

        $this->trackingLogger->info($logData);
    }

    public function logTrait(string $source = ''): void
    {

        $this->logInfo($source);

    }

}
