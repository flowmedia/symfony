<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProductRepository extends ServiceEntityRepository {
    public function __construct( RegistryInterface $registry ) {
        parent::__construct( $registry, Product::class );
    }

    public function getAllProducts() {
        return $this->createQueryBuilder( 'p' )
            ->where( 'p.deleted = :deleted' )
            ->orderBy( 'p.clickBankId', 'ASC' )
            ->getQuery()
            ->getResult();
    }

    public function getProducts($filters)
    {

        $q = $this->createQueryBuilder( 'product' );
        $q->addOrderBy( "product.id", "ASC" );


        if (isset($filters['limit'])) {
            $q->setMaxResults($filters['limit']);
        }

        if (isset($filters['offset'])) {
            $q->setFirstResult($filters['offset']);
        }

        if (isset($filters['like']) && !empty($filters['like'])) {
            $q->andWhere('(product.name LIKE :search OR product.description LIKE :search OR product.click_bank_id LIKE :search)');
            $q->setParameter('search', $filters['like']);
        }

        $results =  $q->getQuery()->getResult();

        return $results;
    }

    public function countProducts($filters = false):int
    {
        $filters = false;
        $q = $this->createQueryBuilder( 'product' );
        $q->select('count(product.id)');


        if (isset($filters['limit'])) {
            $q->setMaxResults($filters['limit']);
        }

        if (isset($filters['offset'])) {
            $q->setFirstResult($filters['offset']);
        }

        if (isset($filters['like']) && !empty($filters['like'])) {
            $q->andWhere('(product.name LIKE :search OR product.description LIKE :search OR product.click_bank_id LIKE :search)');
            $q->setParameter('search', $filters['like']);
        }

        $results = $q->getQuery()->getSingleScalarResult();

        return $results;
    }

}
