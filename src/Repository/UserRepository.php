<?php
namespace App\Repository;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }


    public function getUsers($filters)
    {

        $q = $this->createQueryBuilder( 'user' );
        $q->addOrderBy( "user.id", "ASC" );


        if (isset($filters['limit'])) {
            $q->setMaxResults($filters['limit']);
        }

        if (isset($filters['offset'])) {
            $q->setFirstResult($filters['offset']);
        }

        if (isset($filters['like']) && !empty($filters['like'])) {
            $q->andWhere('(user.username LIKE :search OR user.email LIKE :search OR user.role LIKE :search)');
            $q->setParameter('search', $filters['like']);
        }

        $results =  $q->getQuery()->getResult();

        return $results;
    }

    public function countUsers($filters = false):int
    {
        $filters = false;
        $q = $this->createQueryBuilder( 'user' );
        $q->select('count(user.id)');


        if (isset($filters['limit'])) {
            $q->setMaxResults($filters['limit']);
        }

        if (isset($filters['offset'])) {
            $q->setFirstResult($filters['offset']);
        }

        if (isset($filters['like']) && !empty($filters['like'])) {
            $q->andWhere('(user.username LIKE :search OR user.email LIKE :search OR user.role LIKE :search)');
            $q->setParameter('search', $filters['like']);
        }

        $results = $q->getQuery()->getSingleScalarResult();

        return $results;
    }



}