require('../css/datatable.scss');
const dataTable = require('datatables.net-bs4');

$(function(){
    var oTable = $('#user_list').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "bProcessing": true,
        "bServerSide": true,
        "bStateSave": false,
        "bPaginate": true,
        "iDisplayLength": 10,
        "bLengthChange": true,
        "bSort": false,
        "bInfo" : true,
        "sAjaxSource": "/admin/user_list_ajax",
        "aoColumns": [
            { "bSortable": false, "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" }
        ],
        "fnServerData": function ( sSource, aoData, fnCallback ) {
            // add other filter data from form inputs
            var externalData = $('.users').serializeArray();
            $(externalData).each(function(){
                aoData.push(this);
            });
            $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            } );
        },
        "fnInitComplete": function(){}
    });

    $('#filters select').change(function() {
        oTable._fnAjaxUpdate();
        return false;
    });
});


$(function(){
    var oTable = $('#product_list').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "bProcessing": true,
        "bServerSide": true,
        "bStateSave": false,
        "bPaginate": true,
        "iDisplayLength": 10,
        "bLengthChange": true,
        "bSort": false,
        "bInfo" : true,
        "sAjaxSource": "/admin/product/product_list_ajax",
        "aoColumns": [
            { "bSortable": false, "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" }
        ],
        "fnServerData": function ( sSource, aoData, fnCallback ) {
            // add other filter data from form inputs
            var externalData = $('.product').serializeArray();
            $(externalData).each(function(){
                aoData.push(this);
            });
            $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            } );
        },
        "fnInitComplete": function(){}
    });

    $('#filters select').change(function() {
        oTable._fnAjaxUpdate();
        return false;
    });
});